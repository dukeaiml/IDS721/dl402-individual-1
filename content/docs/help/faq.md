+++
title = "FAQ"
description = "Answers to frequently asked questions."
date = 2021-05-01T19:30:00+00:00
updated = 2021-05-01T19:30:00+00:00
draft = false
weight = 30
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Answers to frequently asked questions."
toc = true
top = false
+++

## Where am I from?

I was born and raised in Shanghai, China. Besides that, I spent one year in AZ, US as exchange student, four years in UVa, VA, for undergraduate, and now living in Durham, NC.

## What's my fun fact?

I miss a tooth.

## What's your hobbies?

- Basketball
- Gaming
- Sports card
- Mechnical Keyboard

## Contact the creator?

Send *Da Lin* an E-mail:

- <justinlinda2001@gmail.com>
