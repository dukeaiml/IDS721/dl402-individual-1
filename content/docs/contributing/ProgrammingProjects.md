+++
title = "Programming Projects"
description = "Here are some projects I did."
date = 2021-05-01T18:10:00+00:00
updated = 2021-05-01T18:10:00+00:00
draft = false
weight = 410
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Here are some projects I did."
toc = true
top = false
+++

### Recipick

👉 [Github repo](https://github.com/Da-Justin-Lin/CS4640-Project)

- Implemented webapp using PHP and JavaScript with Angular as front-end framework.
- Designed and implemented RESTful APIs and ORM migrations supporting various manipulations toward database.

### Landmark Classifier based on CNN

👉 [Github repo](https://github.com/Da-Justin-Lin/CNN_UVa_Landscape)

- Implemented a classifier based on Convolutional Neural Network and UVA landmark dataset (13k records).
- Applied data augmentation by rotating, cropping, and adjusting contrast, which significantly eased the overfitting issue from data side.
- Applied transfer learning by attaching VGG-16 layers on top of the original implementation.
- Improved the model’s validation accuracy by 23% after hyperparameter tuning.
