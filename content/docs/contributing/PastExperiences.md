+++
title = "Past Experiences"
description = "Intern and Research"
date = 2021-05-01T18:20:00+00:00
updated = 2021-05-01T18:20:00+00:00
draft = false
weight = 420
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Intern and Research"
toc = true
top = false
+++

### Primary Research Assistant

👉 [Github repo](https://github.com/Da-Justin-Lin/econ_application)

- Developed an interactive survey website using Python and Django framework, and integrated otree platform.
- Applied machine learning algorithms such as linear regression, forrest method, SVM, to collected data using Sklearn;
Conducted analysis with visualization of results using matplotlib.
- Monitored survey progress and maintained website’s connection and performance.

### Ernst & Young Data Analyst Intern

- Worked in forensic department on investigation of a pre-IPO company to detect potential risk and fraud; implemented scheduled SQL jobs to prepare aggregated reporting.
- Implemented a data pipeline using Python Beam and a scheduled job to keep delivering fraud detection results with 10m+ records input; extracted outlier instances and found more than 20 problems.
- Devised dashboard using Plotly.
