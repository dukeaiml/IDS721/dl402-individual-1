+++
title = "All about me"
description = "AdiDoks is a Zola theme helping you build modern documentation websites, which is a port of the Hugo theme Doks for Zola."
date = 2021-05-01T08:00:00+00:00
updated = 2021-05-01T08:00:00+00:00
draft = false
weight = 10
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = '<img src="../Profile_new.jpeg" width=30%>'
toc = true
top = false
+++

## Education
I graduated from University of Virginia with a degree in both Computer Science and Mathematics. Currently, I'm a MSCS student at Duke University.
* <b> University of Virginia</b>

    B.A. in Computer Science and Mathematics (08/2019 - 05/2023)

* <b> Duke University</b>

    M.S. in Computer Science (08/2023 - present)

## Skills
* <b> Coding Languages </b>

    Java, Python, C++, JavaScript, TypeScript, HTML, CSS, SQL

* <b> Technical skills </b>

    React, Django, Angular, Pytorch/Tensorflow, Scipy/Sklearn, Kubeflow, Cloud Computing


## My Projects

Find out how to contribute to Doks. [Contributing →](../../contributing/how-to-contribute/)
