# Individual Project 1
## Author
Da Lin

## URL:
GitLab: https://dl402-individual-1-dukeaiml-ids721-96648baedc79f1ca25db2db8f079.gitlab.io

Netlify: https://dalin.netlify.app/

Demo Video: https://youtu.be/PFXPzszefK8

## Generating the Website:
1. Create the website by running `zola init <SiteName>`
2. Downolad the theme to the theme directory:
    ```
    cd /themes
    git clone <theme_URL>
    ```
3. Write the content of the website in markdown.
4. Build the website by `zola build`.

### Build and Deploy on GitLab
1. Add the theme submodule to the project 
    ```
    git submodule add <HTTP_THEME_URL> themes/<THEME_NAME>
    ```
2. Setting up the GitLab CI/CD pipeline. Create a file named `.gitlab-ci.yml` in the root directory of the repository and add deployment commands. In the `.gitlab-ci.yml` file, add the following code:
    ```stages:
          - deploy

        default:
          image: debian:stable-slim
          tags:
            - docker

        variables:
          # The runner will be able to pull your Zola theme when the strategy is
          # set to "recursive".
          GIT_SUBMODULE_STRATEGY: "recursive"

          # If you don't set a version here, your site will be built with the latest
          # version of Zola available in GitHub releases.
          # Use the semver (x.y.z) format to specify a version. For example: "0.17.2" or "0.18.0".
          ZOLA_VERSION:
            description: "The version of Zola used to build the site."
            value: ""

        pages:
          stage: deploy
          script:
            - |
              apt-get update --assume-yes && apt-get install --assume-yes --no-install-recommends wget ca-certificates
              if [ $ZOLA_VERSION ]; then
                zola_url="https://github.com/getzola/zola/releases/download/v$ZOLA_VERSION/zola-v$ZOLA_VERSION-x86_64-unknown-linux-gnu.tar.gz"
                if ! wget --quiet --spider $zola_url; then
                  echo "A Zola release with the specified version could not be found.";
                  exit 1;
                fi
              else
                github_api_url="https://api.github.com/repos/getzola/zola/releases/latest"
                zola_url=$(
                  wget --output-document - $github_api_url |
                  grep "browser_download_url.*linux-gnu.tar.gz" |
                  cut --delimiter : --fields 2,3 |
                  tr --delete "\" "
                )
              fi
              wget $zola_url
              tar -xzf *.tar.gz
              ./zola build

          artifacts:
            paths:
              # This is the directory whose contents will be deployed to the GitLab Pages
              # server.
              # GitLab Pages expects a directory with this name by default.
              - public

          rules:
            # This rule makes it so that your website is published and updated only when
            # you push to the default branch of your repository (e.g. "master" or "main").
            - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    ```
Then the pipeline will automatically run on whenever the code is pushed.

### Hosted on Netlify
1. Go to Netlify website and sign in using Gitlab account and link your project on Netlify.
2. Create a file named `netlify.toml` in the root directory of the project.
3. Add deploy commands in `netlify.toml` and push the project onto the GitLab repo. The content of my deployment file:
    ```toml
    [build]
    # This assumes that the Zola site is in a docs folder. If it isn't, you don't need
    # to have a `base` variable but you do need the `publish` and `command` variables.
    publish = "/public"
    command = "zola build"

    [build.environment]
    # Set the version name that you want to use and Netlify will automatically use it.
    ZOLA_VERSION = "0.18.0"

    [context.deploy-preview]
    command = "zola build --base-url $DEPLOY_PRIME_URL"
        ```
4. Host it on Netlify.


## Screenshots: 
### Websites screenshots:
![Alt text](screenshots/Screenshot1.png)
![Alt text](screenshots/Screenshot2.png)
![Alt text](screenshots/Screenshot3.png)
![Alt text](screenshots/Screenshot4.png)
![Alt text](screenshots/Screenshot5.png)

### Hosted screenshots:
Netlify hosted and corresponding url:
![Alt text](screenshots/hosted_netlify.png)
![Alt text](screenshots/netlify_url.png)
Gitlab pipeline working:
![Alt text](screenshots/pipeline.png)
